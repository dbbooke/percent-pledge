Rails.application.routes.draw do
  resources :customers, only: :show do 
    resources :banks, only: %i(create show) do
      put '/transfers' => 'banks#transfer_service'
    end
  end
end
