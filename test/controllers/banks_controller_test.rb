require 'test_helper'

class BanksControllerTest < ActionDispatch::IntegrationTest
	setup do
	 @bank = banks(:one)
	 @bank2 = banks(:two)
	 @customer = customers(:customer1)
	end

	test "should show bank" do
	  @bank = banks(:one)
	  get customer_bank_path(@customer, @bank)
	  assert_response :success
	end

	test "should create bank" do 
		assert_difference("Bank.count") do 
			post customer_banks_url(@customer, params: { name: @bank.name, deposit_amount: @bank.deposit_amount } )
		end

		assert_response :created
	end

	test "should transfer_service" do
		put customer_bank_transfers_url(@customer, @bank2, params: { deposit_account_id: @bank, amount: 200.00 } )
		assert_response :ok
	end
end