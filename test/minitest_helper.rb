ENV['RAILS_ENV'] ||= 'test'
require_relative "../config/environment"
require "minitest/autorun"

Shoulda::Matchers.configure do |config|
  config.integrate do |with|
    with.test_framework :minitest
    with.library :rails
  end
end