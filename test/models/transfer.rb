require "minitest_helper"

class TransferTest < Minitest::Test
  context 'associations' do
    should belong_to(:banks)
  end

  context 'validations' do
    should validate_presence_of(:amount)
  end

  def test_should_require_amount
    transfer = Transfer.new
    refute transfer.valid?
    refute transfer.save
    assert_operator transfer.errors.count, :>, 0
    assert transfer.errors.messages.include?(:amount)
    assert transfer.errors.messages[:amount].include?("can't be blank")
  end
end

