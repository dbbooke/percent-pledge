require "minitest_helper"
class BankTest < Minitest::Test
  context 'associations' do
    should belong_to(:customer)
    should have_many(:transfers)
  end

  context 'validations' do
    should validate_presence_of(:name)
    should validate_presence_of(:deposit_amount)
  end
  
  def test_should_require_name
    bank = Bank.new
    refute bank.valid?
    refute bank.save
    assert_operator bank.errors.count, :>, 0
    assert bank.errors.messages.include?(:name)
    assert bank.errors.messages[:name].include?("can't be blank")
  end

  def test_should_require_deposit_amount
    bank = Bank.new
    refute bank.valid?
    refute bank.save
    assert_operator bank.errors.count, :>, 0
    assert bank.errors.messages.include?(:deposit_amount)
    assert bank.errors.messages[:deposit_amount].include?("can't be blank")
  end
end
