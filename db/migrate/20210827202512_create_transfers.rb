class CreateTransfers < ActiveRecord::Migration[6.1]
  def change
    create_table :transfers do |t|
      t.decimal :amount
      t.integer :bank_id

      t.index :bank_id
      t.timestamps
    end
  end
end
