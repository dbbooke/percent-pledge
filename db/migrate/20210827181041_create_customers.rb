class CreateCustomers < ActiveRecord::Migration[6.1]
  def change
    create_table :customers do |t|
      t.string :name

      t.timestamps
    end

    # Lets add a percision and scale to deal with  currency
    add_column :banks, :deposit_amount, :decimal, :precision => 8, :scale => 2
    add_column :banks, :customer_id, :integer, index: true
    add_index :banks, :customer_id
  end
end
