require 'rails_helper'

RSpec.describe BanksController, type: :controller do 
  describe "POST #create" do
    #Factory bot not working on my local :(
    @customer = Customer.create!(name: "batman")
    params = {
      name: "checkings account",
      deposit_amount: 500.00,
      customer_id: @customer.id
    }

    it "creates a new bank" do
      expect { post :create, params: params }.to change(Bank, :count).by(+1)
      expect(response).to have_http_status :created
    end

    it "creates a bank with the correct attributes" do
      post :create, params: params
      expect(Bank.last).to have_attributes params
    end
  end
end
