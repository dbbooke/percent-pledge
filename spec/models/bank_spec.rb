require 'rails_helper'

RSpec.describe Bank, type: :model do
  it { should belong_to(:customer) } 
  it { should have_many(:transfers) } 
  it { should validate_presence_of(:deposit_amount) }
  it { should validate_presence_of(:name) }
end