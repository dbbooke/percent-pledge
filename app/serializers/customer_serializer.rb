class CustomerSerializer < BaseSerializer
  attributes :id, :name
  lazy_has_many :banks
end
