class TransferSerializer < BaseSerializer
  attributes :id, :amount, :bank_id
end
