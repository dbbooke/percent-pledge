class BankSerializer < BaseSerializer
  attributes :id, :name, :deposit_amount
  belongs_to :customer

  lazy_has_many :transfers,
                serializer: TransferSerializer,
                loader: AmsLazyRelationships::Loaders::Association.new("Bank", :transfers)
end
