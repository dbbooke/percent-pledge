class Bank < ApplicationRecord
  belongs_to :customer, optional: false
  has_many :transfers
  validates_presence_of :deposit_amount, :name
end