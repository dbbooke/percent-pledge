class Transfer < ApplicationRecord
	belongs_to :bank, optional: false
	validates_presence_of :amount
end
