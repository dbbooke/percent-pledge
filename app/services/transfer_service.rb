class TransferService
  def initialize(deposit_account_id, bank_id, amount, customer)
    @deposit_account = deposit_account_id
    @bank_id = bank_id
    @amount = amount.present? ? amount.to_f : 0
    @customer = customer
    @dest_customer = Bank.find(@bank_id).customer_id
  end

  def transfer
    return false if @deposit_account == @bank_id
    return false unless @amount > 0
    return false unless Bank.exists?(@deposit_account) && Bank.exists?(@bank_id)
    banking = Bank.where(id: @deposit_account).pluck(:deposit_amount).reduce(:+) || 0.0
    return false if banking < @amount

    initiate_transfer
  end

  private

  def initiate_transfer
    ActiveRecord::Base.transaction do
      Bank.where(id: @bank_id, customer: @customer).first&.increment!("deposit_amount", by = @amount)
      Bank.where(id: @deposit_account, customer: @dest_customer).first&.decrement!("deposit_amount", by = @amount)
      Transfer.create(amount: @amount, bank_id: @deposit_account)
    end
  end
end