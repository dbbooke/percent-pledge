class ApplicationController < ActionController::API

	private

	def custom_serializer(records, serializer)
      ActiveModelSerializers::SerializableResource.new(records, each_serializer: serializer)
    end
end
