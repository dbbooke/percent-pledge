class CustomersController < ApplicationController
 before_action :find_customer, only: :show
 
 def show
    render json: { data: custom_serializer(@customer, CustomerSerializer) }, status: :ok
 end

 private

 def find_customer
    @customer = Customer.find(params[:id])
 end
end