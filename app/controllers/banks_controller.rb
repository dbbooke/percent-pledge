class BanksController < ApplicationController
    before_action :find_customer, only: [:create, :transfer_service, :show]
    
    def create
      @bank = @customer.banks.new(set_bank_params)

      if @bank.save
        render json: { bank: custom_serializer(@bank, BankSerializer) }, status: :created
      else
        render json: @bank.errors, status: :unprocessable_entity
      end
    end

    def show
        render json: { bank: custom_serializer(@customer.banks, BankSerializer) }
    end

    def transfer_service
    bank = TransferService.new(params[:deposit_account_id],
                                  params[:bank_id],
                                  params[:amount],
                                  params[:customer_id])

      if bank.transfer
        render json: { bank: custom_serializer(@customer.banks, BankSerializer) }, status: :ok
      else
        render json: :error, status: :unprocessable_entity
      end
    end

private

    def set_bank_params
        # Shouldn't need an id because it's implicit but giving an unpermitted param in logs so just putting it here for now
        params.permit(:name, :deposit_amount, :customer_id)
    end

    def find_customer
      @customer = Customer.find(params[:customer_id])
    end
end